% Script to compute the final position of particles for different plds and spawing time
clear all; close all;

np=10;                  % Number of particles
rp= 7;                  % Release point 
dhour=24;               % number of hours per day 
dspawn=3;               % duration of a spawning event 
nspawn=dspawn*dhour;    % number of spawing per episode
tspawn=[1:3:60];        % starting day of  each spawing episode
pld=[3.5:3.5:21]';      % pld array
nfield=3;               % number of fields to red from csv file (lon,lat,depth)
pld_matrix=zeros(np,rp,nfield,1+size(pld,1),nspawn);
nt=(pld*dhour);       % time stamp corresponding to each PLD

% Load file with initial position 

fname=['/mnt/data2/sciascia/LigurianSea_Connectivity/InputData/Ligurian_virtualSamples.csv'];
data=load(fname);
data=data(:,1:3);
data=circshift(data,[0 1]);
data=reshape(data,np,rp,62*dhour,3);
data2=squeeze(data(:,:,1,:));

for itspawn=1:size(tspawn,2)
    for i=1:nspawn
        pld_matrix(:,:,:,1,i)=data2;
    end
for ipld=1:size(nt,1)
    for jspawn=1:nspawn
        i=((tspawn(1,itspawn)-1)*dhour)+jspawn+nt(ipld,1);
        if i>=1000, blank = ''; end
        if i<1000, blank = '0'; end
        if i<100,  blank = '00'; end
        if i<10,   blank = '000'; end
        fname=['/mnt/data2/sciascia/LigurianSea_Connectivity/2004_PassiveParticles_test2/para1000',blank,num2str(i),'.csv'];
        data = load(fname);
        data=reshape(data,np,rp,62*dhour,6);
        data(:,:,:,[2,5,6]) = [];
        pld_matrix(:,:,:,1+ipld,jspawn)=squeeze(data(:,:,(((tspawn(1,itspawn)-1)*dhour)+jspawn),:));
    end
end

endfile=['/mnt/data2/sciascia/LigurianSea_Connectivity/2004_PassiveParticles_test2/Animation/pld2004passive_spawningDay_',num2str(tspawn(1,itspawn)),'.mat'];
save(endfile,'pld_matrix')
pld_matrix=zeros(np,rp,nfield,1+size(pld,1),nspawn);
end
