% Script to compute ligurian sea connectivity for the different experiments. 

clear all; close all;

% define paramentes 

np=10;                      % Number of particles
rp= 7;                      % Release point 
dhour=24;                   % number of hours per day 
dspawn=3;                   % duration of a spawning event 
nspawn=dspawn*dhour;        % number of spawing per episode
tspawn=[1:3:60];            % starting day of  each spawing episode
nfield=3;                   % number of fields to red from csv file (lon,lat,depth)
pld=size([3.5:3.5:21]',1);  % number of plds to consider
ntot=np*nspawn;             % total number of particles released per event 
rdest=7;                    % index of the destination area


% Define sampling coordinates
sample_lon=[8.4461 9.2188 9.3963 9.6322 9.8511 10.3338 10.5469];
sample_lat=[44.2416 44.2980 44.2482 44.1247 44.0227 43.4700 42.8555];

%%define the connectivity area for each sampling location 
    for i=1:rp
        th = 0:pi/50:2*pi;
        r=0.04;
        xc(:,i)= r * cos(th)+sample_lon(i);
        yc(:,i)= r * sin(th)+sample_lat(i);
    end


count=zeros(size(tspawn,2),rp,rdest,pld);
icount=0;

% load first file to analyse 

for  k=1:size(tspawn,2)
     % load file with pld 
     infile=['/mnt/data2/sciascia/LigurianSea_Connectivity/2004_PassiveParticles_test2/Animation/pld2004passive_spawningDay_',num2str(tspawn(1,k)),'.mat'];
     load(infile)
     text=['loading file ',infile];
     disp(text)
     for j =1:pld
         for i= 1:rp
             for ii= 1:rdest
                  for iswn= 1:nspawn
                      for inp=1:np
                         if(inpolygon(pld_matrix(inp,i,2,j+1,iswn),pld_matrix(inp,i,3,j+1,iswn),xc(:,ii),yc(:,ii))==1)
                           icount=icount+1;
                         end
                      end
                  end
                  count(k,i,ii,j)=icount;
                  clear icount
                  icount=0;
             end
         end
     end   
end

count_perc=(count./720)*100;


endfile=['/mnt/data2/sciascia/LigurianSea_Connectivity/2004_PassiveParticles_test2/Animation/connectivity_2004.mat'];
save(endfile,'count','count_perc')
