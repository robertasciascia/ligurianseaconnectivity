% Script to create csv file for LTRANS Simulation 


clear all; close all; 


load ('/mnt/data2/sciascia/LigurianSea_Connectivity/InputData/virtualSample_offshore.mat');


depth_virtualSample=depth_virtualSample-5;
depth_virtualSample=-depth_virtualSample;

dum=zeros(4,(62*24*70));
dum_lon=[lon_virtualSample(1,:)'; lon_virtualSample(2,:)'; lon_virtualSample(3,:)'; lon_virtualSample(4,:)'; lon_virtualSample(5,:)'; lon_virtualSample(6,:)'; lon_virtualSample(7,:)'];
dum_lat=[lat_virtualSample(1,:)'; lat_virtualSample(2,:)'; lat_virtualSample(3,:)'; lat_virtualSample(4,:)'; lat_virtualSample(5,:)'; lat_virtualSample(6,:)'; lat_virtualSample(7,:)'];
dum_depth=[depth_virtualSample(1,:)'; depth_virtualSample(2,:)'; depth_virtualSample(3,:)'; depth_virtualSample(4,:)'; depth_virtualSample(5,:)'; depth_virtualSample(6,:)'; depth_virtualSample(7,:)'];

dum_lon2=repmat(dum_lon,(24*62),1);
dum_lat2=repmat(dum_lat,(24*62),1);
dum_depth2=repmat(dum_depth,(24*62),1);

clear dum_depth dum_lon dum_lat


for i=1:(24*62)

    dum_time=0+3600*(i-1);
    dum_time2=repmat(dum_time,70,1);

    time((1+(i-1)*70):(i*70),1)=dum_time2;

end



dum(1,:)=dum_lon2;
dum(2,:)=dum_lat2;
dum(3,:)=dum_depth2;
dum(4,:)=time;

   dlmwrite('/mnt/data2/sciascia/LigurianSea_Connectivity/InputData/Ligurian_virtualSamples_offshore.csv',dum','delimiter','\t','precision','%.4f')
