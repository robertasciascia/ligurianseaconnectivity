%shows samplig location of
%Paramuricea clavata in the Ligurian Sea
%on LIME-ROMS grid
%
% RS
%
clear all; close all;
addpath(genpath('/mnt/data2/sciascia/utilities/'))
domain = 'lig1kmAuto';

expt = 0;

expstr = num2str(expt);
format long;
indir = ['/mnt/iscsi/ROMS/lig1kmAuto_Ott17/Exp',expstr,'/Out_data'];
inname = ['/ocean_his_0002.nc'];
fileIn = [indir,inname];
disp(['  processing ',fileIn]);
%retrieve grid info only the first time
gExist = exist('G','var');


if(~gExist)
    G = get_roms_grid(fileIn,1);
end

lon = G.lon_rho;
lat = G.lat_rho;
bathy=G.h;
ii=find(bathy==10);
bathy(ii)=NaN;
clear ii 

% interpolate bathymetry on a finer grid 
x_hres=min(min(lon)):0.005:max(max(lon));
y_hres=min(min(lat)):0.005:max(max(lat));
[lon_hres,lat_hres]=meshgrid(x_hres,y_hres);
bathy_hres=interp2(lat,lon,bathy,lat_hres,lon_hres);

% Define sampling coordinates
sample_lon=[8.4461 8.4461 9.2188 9.2188 9.3963 9.3963 9.6322 9.6322 9.8511 9.8505 10.3338 10.3338 10.5469 10.5469];
sample_lat=[44.2416 44.2416 44.2980 44.2980 44.2482 44.2482 44.1247 44.1247 44.0227 44.0238 43.4700 43.4700 42.8555 42.8555];
sample_name{1}='Bergeggi';sample_name{2}= 'Portofino'; sample_name{3}='Sestri Levante';sample_name{4}='Punta Mesco';sample_name{5}='Portovenere';sample_name{6}='Calafuria';sample_name{7}='Cerboli Island';
nstat=7;
skip=1;


for i=1:nstat
    is=0;
%%%%%%Define Axis limits
    minlat=min(sample_lat((i*2)-1:i*2));
    minlon=min(sample_lon((i*2)-1:i*2));

    maxlat=max(sample_lat((i*2)-1:i*2));
    maxlon=max(sample_lon((i*2)-1:i*2));
%%%%%%%%%Find Closest point on the grid to original salmpling location 

    dum=abs(lon_hres(1,:)-sample_lon((i*2)-1));
    jj=find(dum==min(dum));
    dum2=abs(lat_hres(:,jj)-sample_lat((i*2)-1));
    ii=find(dum2==min(dum2));

%%%%%%%%define area in which to select points  
    th = 0:pi/50:2*pi;
    r=0.04;
    xc = r * cos(th) + lon_hres(ii,jj);
    yc = r * sin(th) + lat_hres(ii,jj);
%%%%%%% Plot data%%%%%%
    lon(G.mask_rho == 0) = NaN;
    lat(G.mask_rho == 0) = NaN;

%%%%%%%% Find and plot points within polygon with 25<h<45   
    for ipol=1:size(lon_hres,1)
       for jpol=1:size(lon_hres,2)
            if(bathy_hres(ipol,jpol)< 40.00 && bathy_hres(ipol,jpol)>=25.00)
               if(inpolygon(lon_hres(ipol,jpol),lat_hres(ipol,jpol),xc,yc)==1)
                    xp(is+1)=ipol;
                    yp(is+1)=jpol;
                    is=is+1;
		end
            end
        end
    end
    
%%%%%%%%%% Compute distance from the sampling point and sort
    for id=1:size(xp,2)
        X=[lon_hres(xp(id),yp(id)),lat_hres(xp(id),yp(id));lon_hres(ii,jj), lat_hres(ii,jj)];                                           
	distance_Sampling(id)=pdist(X);
    end
    [sorted_distance, index_distance]=sort(distance_Sampling,'ascend');
    index_distance=index_distance(1:10);

%%%%%%%%%% Plot Data %%%%%%%%
    figure(i)
    set(gcf,'Position', [345 3 655 721],'PaperPositionMode','auto')
    subplot(2,1,1)
    pcolor(lon_hres,lat_hres,-bathy_hres);shading interp; colormap(gray);colorbar;
    caxis([-100 0])
    hold on

    plot(lon_hres(1:skip:end,1:skip:end),lat_hres(1:skip:end,1:skip:end),'.b');
    axis([minlon-0.25 maxlon+0.25 minlat-0.25 maxlat+0.24])
    plot(sample_lon((i*2)-1),sample_lat((i*2)-1),'or','MarkerSize',8)
    plot(sample_lon(i*2),sample_lat(i*2),'ok','MarkerSize',8)
    h = plot(xc, yc);
    xlabel('Lon')
    ylabel('Lat')
    title(sample_name(i))
    for ip=1:10
        plot(lon_hres(xp(index_distance(ip)),yp(index_distance(ip))),lat_hres(xp(index_distance(ip)),yp(index_distance(ip))),'oy')
        lat_virtualSample(i,ip)=lat_hres(xp(index_distance(ip)),yp(index_distance(ip)));
        lon_virtualSample(i,ip)=lon_hres(xp(index_distance(ip)),yp(index_distance(ip)));
        depth_virtualSample(i,ip)=bathy_hres(xp(index_distance(ip)),yp(index_distance(ip)));
    end
    hold off;
   
    subplot(2,1,2)
    pcolor(lon,lat,-bathy);shading interp; colormap(gray);colorbar;
    caxis([-100 0])
    hold on;
    plot(lon(1:skip:end,1:skip:end),lat(1:skip:end,1:skip:end),'.r','MarkerSize',6);
    axis([minlon-0.25 maxlon+0.25 minlat-0.25 maxlat+0.24])
    plot(sample_lon((i*2)-1),sample_lat((i*2)-1),'or','MarkerSize',8)
    plot(sample_lon(i*2),sample_lat(i*2),'ok','MarkerSize',8)   
    for ip=1:10
        plot(lon_virtualSample(i,ip),lat_virtualSample(i,ip),'oy')
    end 
    hold off;
    xlabel('Lon')
    ylabel('Lat') 
    clear ii jj xc yc ipol jpol is xp yp ip distance_Sampling X index_distance sorted_distance 
 

end


save -mat virtualSample.mat lon_virtualSample lat_virtualSample depth_virtualSample
