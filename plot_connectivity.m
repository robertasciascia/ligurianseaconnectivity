% script to plot connectivitu matrix
% the user needs to input some data


clear all; close all;



% get some inputs from the user 

yrstr=input('Which year you want to plot [yyyy] ','s');

% load file 

infile=['/mnt/data2/sciascia/LigurianSea_Connectivity/',yrstr,'_PassiveParticles_test2/Animation/connectivity_',yrstr,'.mat'];

load(infile)

pldstr=input('Which pld you want to anayze, in days [3.5  7  10.5  14  17.5 21] ');

switch pldstr

	case 3.5
	     ipld = 1;
	case 7
	     ipld = 2;
	case 10.5
	     ipld = 3;
	case 14
	     ipld = 4;
	case 17.5
	     ipld = 5;
	case 21
	     ipld = 6;

end

tspawn=[1:3:60];     % starting time of eache spawning event


for i=1:size(tspawn,2)
	fg=figure(i);
        set(fg,'Position', [360 91 697 607],'PaperPositionMode','auto')
	cdum=squeeze(count(i,:,:,ipld));
        cdum(find(cdum==0))=NaN; 
        imagesc([1:1:7],[1:1:7],cdum, 'AlphaData',~isnan(cdum));shading flat;colorbar
	axis xy
	set(gca, 'YTickLabel',{'BER' 'POF' 'SES' 'PM' 'PV' 'CAL' 'CER'},'FontSize',10);
	set(gca, 'XTickLabel',{'BER' 'POF' 'SES' 'PM' 'PV' 'CAL' 'CER'},'FontSize',10);
        colormap(parula)
        caxis([0 720])    
	set(gca,'FontSize',10)
	ylabel('Source','Fontsize',12)
        xlabel('Target','Fontsize',12)
        % savefile 
        outfig=['/mnt/data2/sciascia/LigurianSea_Connectivity/',yrstr,'_PassiveParticles_test2/Animation/connectivity',yrstr,'_pld_',num2str(pldstr),'_',num2str(tspawn(i)),'.png'];
        print('-dpng', outfig) 
        clear cdum      
end
