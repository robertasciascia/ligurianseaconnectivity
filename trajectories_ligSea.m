% MATLAB script for creating  LTRANS v.2 trajectories 


clear all
close all


lat_roms=ncread('/mnt/data2/sciascia/LigurianSea_Connectivity/InputData/lig1kmAuto_Ott17_grd.nc','lat_rho');
lon_roms=ncread('/mnt/data2/sciascia/LigurianSea_Connectivity/InputData/lig1kmAuto_Ott17_grd.nc','lon_rho');
h=ncread('/mnt/data2/sciascia/LigurianSea_Connectivity/InputData/lig1kmAuto_Ott17_grd.nc','h');
mask=ncread('/mnt/data2/sciascia/LigurianSea_Connectivity/InputData/lig1kmAuto_Ott17_grd.nc','mask_rho');


np=10;
rp=7;
dd=30;
ddstr=num2str(dd);
dt=1; % save interval in hours
nt=(dd*(24/(dt))); 
lat=zeros(np*rp*62*24,nt);
lon=zeros(np*rp*62*24,nt);
depth=zeros(np*rp*62*24,nt);

indir='/mnt/data2/sciascia/LigurianSea_Connectivity/2004_PassiveParticles/';


for i = 2:nt+1
    if i>=1000, blank = ''; end
    if i<1000, blank = '0'; end
    if i<100,  blank = '00'; end
    if i<10,   blank = '000'; end
    fname = [indir,'para1000',blank,num2str(i),'.csv'];
    data = load(fname);
    lat(:,i-1)=squeeze(data(:,4));
    lon(:,i-1)=data(:,3);
    depth(:,i-1)=data(:,1);
end

%for i=1:rp
%    mean_lat(i,:)=mean(lat(1+np*(i-1):np*i,:),1);
%    mean_lon(i,:)=mean(lon(1+np*(i-1):np*i,:),1);
    
%end 
     
%    fig1=figure(1);
  

%    set(gcf,'Position', [141 147 1083 558],'PaperPositionMode','auto')
%    pcolor(lon_roms,lat_roms,mask), shading flat
%    colormap(gray)
    
%    hold on
    
%    for jj=1:rp
%     plot(mean_lon(jj,1:end),mean_lat(jj,1:end),'-','Color',[0.4 0.2 1]+[0.0024*jj 0 0],'LineWidth',0.5)
%     plot(lon((1+((jj-1)*np)):(jj*np),:),lat((1+((jj-1)*np)):(jj*np),:),'-','Color',[0.53 0.15 0.34]+[(jj*0.0015) (jj*0.0008) (jj*0.0006)],'LineWidth',0.25)
%    end
%    hold off
  
%    xlabel('Lon','Fontsize',16)
%    ylabel('Lat','Fontsize',16)
%    set(gca, 'Fontsize',16)
    
%    exportfile = [indir,'Figures/trajectories_',ddstr,'.png'];
    
%    print(fig1,'-dpng', exportfile)
