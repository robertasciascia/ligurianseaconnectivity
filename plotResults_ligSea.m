% MATLAB script for creating animations of LTRANS v.2
% Original code by Yong Kim  
% Adapted for LTRANS v.2 by Elizabeth North


clear all; close all;

lat=ncread('/mnt/data2/sciascia/LigurianSea_Connectivity/InputData/lig1kmAuto_Ott17_grd.nc','lat_rho');
lon=ncread('/mnt/data2/sciascia/LigurianSea_Connectivity/InputData/lig1kmAuto_Ott17_grd.nc','lon_rho');
h=ncread('/mnt/data2/sciascia/LigurianSea_Connectivity/InputData/lig1kmAuto_Ott17_grd.nc','h');
mask=ncread('/mnt/data2/sciascia/LigurianSea_Connectivity/InputData/lig1kmAuto_Ott17_grd.nc','mask_rho');
FramesFolder = '/mnt/data2/sciascia/LigurianSea_Connectivity/2004_PassiveParticles_test3/Animation/';
endfile='/mnt/data2/sciascia/LigurianSea_Connectivity/2004_PassiveParticles_test3/Animation/endLocation.mat';

% initialize variables 


APPEND=1;

np=10;
rp=7;
ligt=np*rp;
days=125;
nstep=3600;
nfield=6;
nseed=62;
dhour=24;
pld=15;
nt=1848;

for iend=1:(nseed*dhour)
    npld(iend,1)=iend+(pld*dhour);
    npld(iend,2)=iend;
end


if(APPEND)

  load(endfile)

else 
  locend=zeros(np,rp,nseed*dhour,nfield);
  icount=1;
end 


for i = 1811:nt
    if i>=1000, blank = ''; end
    if i<1000, blank = '0'; end
    if i<100,  blank = '00'; end
    if i<10,   blank = '000'; end
    fname = ['/mnt/data2/sciascia/LigurianSea_Connectivity/2004_PassiveParticles_test3/para1000',blank,num2str(i),'.csv'];
    data = load(fname);
    if (i<=(nseed*dhour+1))
      nend=(i-1)*ligt;
      dum=data(1:nend,:);
      dum=reshape(dum,np,rp,(i-1),nfield);
    else

      nend=(nseed*dhour*ligt);
      dum=data(1:nend,:);
      dum=reshape(dum,np,rp,nseed*dhour,nfield);
    end

    set(gcf,'Color','w','Position', [-41 87 1072 637],'PaperPositionMode','auto')
% plot mask
    contour(lon,lat, h,[20 50 100 150 200 300 400 500 1000 2000],'--k')  
    hold on
    contour(lon,lat,mask,'Color','k','LineWidth',1)
    axis([7.5 12 42 44.5]) 

      if ((i-1) < npld(icount,1))
	    plot(squeeze(dum(:,1,:,3)),squeeze(dum(:,1,:,4)),'.','Color',[76/256 0 153/256],'MarkerSize',14)
      	    plot(squeeze(dum(:,2,:,3)),squeeze(dum(:,2,:,4)),'.','Color',[0 102/256 0],'MarkerSize',14)
      	    plot(squeeze(dum(:,3,:,3)),squeeze(dum(:,3,:,4)),'.','Color',[255/256 51/256 51/256],'MarkerSize',14)
      	    plot(squeeze(dum(:,4,:,3)),squeeze(dum(:,4,:,4)),'.','Color',[0 204/256 204/256],'MarkerSize',14)
            plot(squeeze(dum(:,5,:,3)),squeeze(dum(:,5,:,4)),'.','Color',[255/256 128/256 0],'MarkerSize',14)
    	    plot(squeeze(dum(:,6,:,3)),squeeze(dum(:,6,:,4)),'.','Color',[0 102/256 204/256],'MarkerSize',14)
      	    plot(squeeze(dum(:,7,:,3)),squeeze(dum(:,7,:,4)),'.','Color',[255/256 0 255/256],'MarkerSize',14)
      elseif ( (i-1) ==npld(icount,1))
            ii=find(npld(:,1)==(i-1));
            locend(:,:,npld(ii,2),:)=dum(:,:,npld(ii,2),:);
            plot(squeeze(locend(:,1,1:npld(icount,2),3)),squeeze(locend(:,1,1:npld(icount,2),4)),'.','Color',[204/256 153/256 255/256],'MarkerSize',14)
            plot(squeeze(locend(:,2,1:npld(icount,2),3)),squeeze(locend(:,2,1:npld(icount,2),4)),'.','Color',[153/256 255/256 153/256],'MarkerSize',14)
            plot(squeeze(locend(:,3,1:npld(icount,2),3)),squeeze(locend(:,3,1:npld(icount,2),4)),'.','Color',[255/256 153/256 153/256],'MarkerSize',14)
            plot(squeeze(locend(:,4,1:npld(icount,2),3)),squeeze(locend(:,4,1:npld(icount,2),4)),'.','Color',[204/256 255/256 255/256],'MarkerSize',14)
            plot(squeeze(locend(:,5,1:npld(icount,2),3)),squeeze(locend(:,5,1:npld(icount,2),4)),'.','Color',[255/256 204/256 153/256],'MarkerSize',14)
            plot(squeeze(locend(:,6,1:npld(icount,2),3)),squeeze(locend(:,6,1:npld(icount,2),4)),'.','Color',[153/256 204/256 255/256],'MarkerSize',14)
            plot(squeeze(locend(:,7,1:npld(icount,2),3)),squeeze(locend(:,7,1:npld(icount,2),4)),'.','Color',[255/256 204/256 255/256],'MarkerSize',14)
            icount=icount+1;
            plot(squeeze(dum(:,1,npld(icount,2):end,3)),squeeze(dum(:,1,npld(icount,2):end,4)),'.','Color',[76/256 0 153/256],'MarkerSize',14)
            plot(squeeze(dum(:,2,npld(icount,2):end,3)),squeeze(dum(:,2,npld(icount,2):end,4)),'.','Color',[0 102/256 0],'MarkerSize',14)
            plot(squeeze(dum(:,3,npld(icount,2):end,3)),squeeze(dum(:,3,npld(icount,2):end,4)),'.','Color',[255/256 51/256 51/256],'MarkerSize',14)
            plot(squeeze(dum(:,4,npld(icount,2):end,3)),squeeze(dum(:,4,npld(icount,2):end,4)),'.','Color',[0 204/256 204/256],'MarkerSize',14)
            plot(squeeze(dum(:,5,npld(icount,2):end,3)),squeeze(dum(:,5,npld(icount,2):end,4)),'.','Color',[255/256 128/256 0],'MarkerSize',14)
            plot(squeeze(dum(:,6,npld(icount,2):end,3)),squeeze(dum(:,6,npld(icount,2):end,4)),'.','Color',[0 102/256 204/256],'MarkerSize',14)
            plot(squeeze(dum(:,7,npld(icount,2):end,3)),squeeze(dum(:,7,npld(icount,2):end,4)),'.','Color',[255/256 0 255/256],'MarkerSize',14)
            
       end
    hold off
  
    clear data

    %time counter
    simtime = ((i-1)*nstep)/3600;
    pt=sprintf('T=%4.1f hours',simtime);
    title(pt,'FontSize',16)
    xlabel('Lon','Fontsize',16)
    ylabel('Lat','Fontsize',16)
    zlabel('Depth','Fontsize',16)
    set(gca, 'Fontsize',16)
    

    exportfile = [FramesFolder,'para1000',blank,num2str(i), '.jpg']
    
    
    print('-djpeg', exportfile)

end
save(endfile,'locend','icount') 
